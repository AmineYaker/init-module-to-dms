class ZipJoinKey:
    def __init__(self, table, zip):
        self.table = table
        self.zip = zip

    def asJson(self):
        return f"""
        {{
            "table": "{self.table}",
            "zip": "{self.zip}"
        }}"""


class ZipJoin:
    def __init__(self, raw_data):
        self.raw_data = raw_data
        self.fact_table_name = raw_data["factTable"]
        self.fact_table_array = raw_data["label"]
        self.zip_table_name = raw_data["zipTable"]
        self.zip_label = raw_data["zipLabel"]
        self.keys = [
            ZipJoinKey(jk["factLabel"], jk["zipLabel"]) for jk in raw_data["joinKeys"]
        ]
        self.file_name = f"{self.fact_table_name}-{self.zip_table_name}.json"

    def asJson(self):
        return f"""{{
    "factTable": {{
        "name": "{self.fact_table_name}",
        "arrayFact": "{self.fact_table_array}"
    }},
    "zipTable": {{
        "name": "{self.zip_table_name}",
        "label": "{self.zip_label}"
    }},
    "joinKeys": [{",".join([k.asJson() for k in self.keys])}
    ]
}}
"""


def get_zip_joins(data):
    joins = []
    for raw_data in data["definition"]["arrays"]:
        joins.append(ZipJoin(raw_data))
    return joins

import os

def dumpZipJoins(data, save_path):
    joins = get_zip_joins(data)
    if not os.path.exists(save_path):
        os.mkdir(save_path)
        for join in joins:
            with open(os.path.join(save_path, join.file_name), "w") as file:
                file.write(join.asJson())