class DenormalizedJoin:
    def __init__(self, fact_table_name, dict_table_name):
        self.fact_table_name = fact_table_name
        self.dict_table_name = dict_table_name
        self.file_name = f"{self.fact_table_name}-{self.dict_table_name}.json"

    def asJson(self):
        return f"""{{
    "factTable": "{self.fact_table_name}",
    "dictionary": "{self.dict_table_name}"
}}
"""


class Setting:
    def __init__(self, raw_data):
        self.raw_data = raw_data
        self.dictionaries = (
            raw_data["dictionaries"] if raw_data["dictionaries"] is not None else []
        )


def list_fact_table_names(data):
    return data["definition"]["tables"]


def get_fact_table_settings(data, fact_table_name):
    for raw_setting in data["ingestionSettings"]:
        if raw_setting["table"] == fact_table_name:
            return Setting(raw_setting)
    return None


def get_denormalized_joins(data):
    joins = []
    for fact_table_name in list_fact_table_names(data):
        setting = get_fact_table_settings(data, fact_table_name)
        if setting is None:
            raise RuntimeError(f"fact table {fact_table_name} don't have setting.")
        if setting.dictionaries is None:
            raise RuntimeError(f"fact table {fact_table_name} don't have dictionaries.")
        for dict_table_name in setting.dictionaries:
            joins.append(DenormalizedJoin(fact_table_name, dict_table_name))
    return joins

import os

def dumpDenormalizedJoins(data, save_path):
    joins = get_denormalized_joins(data)
    if not os.path.exists(save_path):
        os.mkdir(save_path)
        for join in joins:
            with open(os.path.join(save_path, join.file_name), "w") as file:
                file.write(join.asJson())
