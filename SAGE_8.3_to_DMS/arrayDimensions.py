import os
import json

from facttables import get_columns


def get_fact_table_names(data, column_name):
    fact_table_names = set()
    for fact_table_name in data["definition"]["tables"]:
        columns = get_columns(fact_table_name, data)
        if column_name in [c["name"] for c in columns]:
            fact_table_names.add(fact_table_name)
    return fact_table_names


class ArrayDimension:
    def __init__(self, array, label):
        self.array = array
        self.label = label

    def as_dict(self):
        return  {"label": self.label, "array": self.array}


class ArrayDimensions:
    def __init__(self, fact_table_name, array_dimensions):
        self.fact_table_name = fact_table_name
        self.array_dimensions = array_dimensions

    def as_dict(self):
        return {"dimensions": [ad.as_dict() for ad in self.array_dimensions]}


def get_array_dimensions(data, array_dim_data):
    fact_table_names = set()
    array_dimensions = [
        ArrayDimension(d["array"], d["label"]) for d in array_dim_data["dimensions"]
    ]
    for array in [ad.array for ad in array_dimensions]:
        fact_table_names = fact_table_names.union(get_fact_table_names(data, array))
    if len(fact_table_names) == 0:
        raise Exception(f"Array dimensions don't have any table.")
    return ArrayDimensions(list(fact_table_names)[0], array_dimensions)

def get_array_dimensions_bodies(data, fact_table_name):
    array_dimensions_list = [
        get_array_dimensions(data, array_dim_data)
        for array_dim_data in data["definition"]["arraysDimensions"]
    ]
    return map(
        lambda x: x.as_dict(),
        filter(lambda x: x.fact_table_name == fact_table_name, array_dimensions_list),
    )
