import json 
import parseString
import os.path 

with open('SAGE_v8_3_onprem.json','r') as f:
    data = json.load(f)



## CUSTOMIZING Ingestion for TPM
definition = data['definition']
ingestion_settings = data['ingestionSettings']
def searchIS(name, ings):
    return [element for element in ings if element['table'] == name]
def searchInitTunicityKey(name, initTs):
    return[element for element in initTs if element['table'] == name and element['key'] == 'unicityKey']
creates = data['creates']
factTables =[]
dateCol = {'name': 'Date', 'dataType': 'Date'}
commitCol = {'name': 'commit', 'dataType': 'Int64'}
tablenames = definition['tables']
initT = data['initT']
res = []
## ALL TABLES ARE VERSIONED , partiotioned by Date, ordered by commi
for name in tablenames:
        
    cust = {}
    cols = []
    ings = searchIS(name, ingestion_settings)[0]
    insertQuery = ings["insertQuery"]

    searchName = 'shard_' + name + ' '
    statement = parseString.find_create_statement(searchName, creates)
    parsedCols = parseString.parse_table_string(statement)
    cols = parsedCols[parsedCols.index(dateCol) + 1 : parsedCols.index(commitCol)]
    custImages = parseString.parse_customizing_images(insertQuery)
    fileCols = parseString.parse_file_cols(insertQuery)
    
    print(cols[len(cols) - 1]["name"] + ':' + custImages[len(custImages) - 1])
    ## todo: figure out case by case the mismatch 
    colWithImage = zip(cols, custImages)
    mapping = {}
    for col, img in colWithImage:
        if (col != dateCol):
            mapping[col['name']] = img
    cust = {'fileColumns': fileCols, 'columnMapping': mapping, 'tableName': name }
    res.append(cust)

wd = os.getcwd()
table_dir = 'ingestion_customization'
save_path = os.path.join(wd, table_dir)
os.mkdir(save_path)

for cust in res:
    fname = cust['tableName'] + '.json'
    completeName = os.path.join(save_path, fname)
    with open(completeName, "w") as outfile:
       json.dump(cust, outfile, indent= 4)

