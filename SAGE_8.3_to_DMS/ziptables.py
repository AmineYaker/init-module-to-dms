import json 
import parseString
import os

def dumpZipTables(data, save_path):
    definition = data['definition']
    creates = data['creates']
    zipjoins = definition['arrays']

    ziptables = []

    if not os.path.exists(save_path):
        os.mkdir(save_path)

    for zipjoin in zipjoins:
        joinKeys = []
        zipName = zipjoin['zipTable']
        statement = parseString.find_create_statement(zipName, creates)
        parsedCols = parseString.parse_table_string(statement)

        joins = zipjoin['joinKeys']
        for join in joins:
            joinKeys.append(join['zipLabel'])
        ziptables.append({'name' : zipjoin['zipTable'], 'database': 'default', 'columns' : parsedCols ,'technical': {'orderingKey' : joinKeys} })


    for zipTable in ziptables:
            fname = zipTable['name'] + '.json'
            completeName = os.path.join(save_path, fname)
            with open(completeName, "w") as outfile:
                json.dump(zipTable, outfile, indent= 4)
