import os 
import json 
import parseString

def get_columns(fact_table_name, data):
    def searchIS(name, ings):
        return [element for element in ings if element['table'] == name]
    
    ingestion_settings = data['ingestionSettings']
    creates = data['creates']
    dateCol = {'name': 'Date', 'dataType': 'Date'}
    commitCol = {'name': 'commit', 'dataType': 'Int64'}
    ings = searchIS(fact_table_name, ingestion_settings)[0]
    facts = ings['facts']
    searchName = 'shard_' + fact_table_name + ' '
    statement = parseString.find_create_statement(searchName, creates)
    parsedCols = parseString.parse_table_string(statement)

    cols = parsedCols[parsedCols.index(dateCol) : parsedCols.index(commitCol)]
    for col in cols:
        if col['name'] in facts:
            col['isFact'] = True
    return cols

def dump(data, secondary_cluster):
    
    definition = data['definition']
    ingestion_settings = data['ingestionSettings']
    def searchInitTunicityKey(name, initTs):
        return[element for element in initTs if element['table'] == name and element['key'] == 'unicityKey']
    
    factTables =[]
    tablenames = definition['tables']
    initT = data['initT']

    ## ALL TABLES ARE VERSIONED , partiotioned by Date, ordered by commit
    for name in tablenames:
        cols = get_columns(name, data)
        unicityKey = searchInitTunicityKey(name, initT)[0]['value'].split(",")
        hyb_filter = {'column': 'IsCloudApproved', 'envs': {secondary_cluster : ['Y']}} if any(['IsCloudApproved' == col['name'] for col in cols]) else None
        factTables.append({'name' : name, 'database': 'default','columns' : cols, 'unicityKey': unicityKey, 'technical': {'partitioningKey': ['Date']}, 'filter': hyb_filter})


    wd = os.getcwd()
    table_dir = 'tables_dumps'
    save_path = os.path.join(wd, table_dir)
    os.mkdir(save_path)

    for table in factTables:
       fname = table['name'] + '.json'
       completeName = os.path.join(save_path, fname)
       with open(completeName, "w") as outfile:
           json.dump(table, outfile, indent= 4)

