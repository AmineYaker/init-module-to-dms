import json 
import parseString
import os.path 



def dump_customizations(data, save_path):
    definition = data['definition']
    ingestion_settings = data['ingestionSettings']
    def searchIS(name, ings):
        return [element for element in ings if element['table'] == name]
    creates = data['creates']
    dateCol = {'name': 'Date', 'dataType': 'Date'}
    commitCol = {'name': 'commit', 'dataType': 'Int64'}
    tablenames = definition['tables']
    res = []
    
    if not os.path.exists(save_path):
        os.mkdir(save_path)


    ## ALL TABLES ARE VERSIONED , partiotioned by Date, ordered by commi
    for name in tablenames:
            
        cust = {}
        cols = []
        ings = searchIS(name, ingestion_settings)[0]
        insertQuery = ings["insertQuery"]

        searchName = 'shard_' + name + ' '
        statement = parseString.find_create_statement(searchName, creates)
        parsedCols = parseString.parse_table_string(statement)
        cols = parsedCols[parsedCols.index(dateCol) + 1 : parsedCols.index(commitCol)]
        custImages = parseString.parse_customizing_images(insertQuery)
        fileCols = parseString.parse_file_cols(insertQuery)
        
        colWithImage = zip(cols, custImages)
        mapping = {}
        for col, img in colWithImage:
            if (col != dateCol):
                mapping[col['name']] = img
        cust = {'fileColumns': fileCols, 'columnMapping': mapping, 'tableName': name }
        res.append(cust)

    for cust in res:
        fname = cust['tableName'] + '.json'
        completeName = os.path.join(save_path, fname)
        with open(completeName, "w") as outfile:
           json.dump(cust, outfile, indent= 4)

