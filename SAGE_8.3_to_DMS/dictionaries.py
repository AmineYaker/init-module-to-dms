import json 
import parseString
import os

def dumpDictionaries(data, save_path):
    definition = data['definition']
    decorationsMap = definition['dictionaries']
    result_dicts = []
    dicts = []

    if not os.path.exists(save_path):
        os.mkdir(save_path)

    for deco in decorationsMap.values():
        dicts.append(deco['dict'])

    ##Done we have dict names
    dictionaries = list(set(dicts))

    ingestion_settings = data['ingestionSettings']

    dicts_from_sds = []

    for ings in ingestion_settings:
        if 'idName' in ings:
            result_dicts.append({'name': ings['idName'], 'database': 'default', 'joinKey': ings['joinKeys']})


    creates = data['creates']
    for dict in result_dicts:
        dictName = 'default.' + dict['name'] + 'Receiver'
        statement = parseString.find_create_statement(dictName, creates)
        parsedCols = parseString.parse_table_string(statement)
        dict['columns'] = parsedCols
        if 'Date' not in dict['joinKey']:
            parsedCols.remove({'name': 'Date', 'dataType': 'Date'})

    for dictionary in result_dicts:
        fname = dictionary['name'] + '.json'
        completeName = os.path.join(save_path, fname)
        with open(completeName, "w") as outfile:
            json.dump(dictionary, outfile, indent= 4)

def dictNames(data):
    ingestion_settings = data['ingestionSettings']
    dict_names = []
    for ings in ingestion_settings:
        if 'idName' in ings:
            dict_names.append(ings['idName'])
    return dict_names

