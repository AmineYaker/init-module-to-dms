import dictionaries

def clean_empty_strings(front_dimensions):
    cleaned = {}
    for key, value in front_dimensions.items():
        if value["alias"] == "":
            del value["alias"]
        if value["description"] == "":
            del value["description"]
        cleaned[key] = value
    return cleaned



def get_front_dimensions_bodies(data):
    forbidden_keys = []
    forbidden_keys = dictionaries.dictNames(data)
    legit_fd = data["definition"]["dimensions"]
    for fk in forbidden_keys:
        del legit_fd[fk]
    return clean_empty_strings(legit_fd)