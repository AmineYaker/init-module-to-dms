import requests
import json
import dictionaries

headers = {"Content-Type": "application/json; charset=utf-8"}


def check_connection(address):
    response = requests.get(f"{address}/ping")
    if response.status_code != 200:
        raise Exception(f"can't connect to {address}")


def import_dms(address, dms_dump):
    module_name = dms_dump["module"]
    create_modules(address, module_name)

    print("### Create fact tables ###")
    for info in dms_dump["tables"]["fact"]:
        table_name = info["body"]["name"]
        print(f"\t- Create fact table {table_name}")
        create_tables_fact(address, module_name, info["body"])
        print(f"\t\t- Create ingestion settings.")
        create_ingestion_settings(
            address, module_name, table_name, info["ingestion-settings"]
        )
        for dimensions_array_info in info["dimensions"]["array"]:
            print(f"\t\t- Create dimension array.")
            create_dimensions_array(
                address, module_name, table_name, dimensions_array_info["body"]
            )

    print("### Create dictionaries ###")
    for info in dms_dump["tables"]["dict"]["denormalized"]:
        print(f"\t- Create dictionary {info['body']['name']}")
        create_tables_dict_denormalized(address, module_name, info["body"])

    print("### Create zips ###")
    for info in dms_dump["tables"]["zip"]["external"]:
        create_tables_zip(address, module_name, info["body"])

    print("### Create denormalized joins ###")
    for info in dms_dump["joins"]["dict"]["denormalized"]:
        create_joins_dict_denormalized(address, module_name, info["body"])

    print("### Create zip joins ###")
    for info in dms_dump["joins"]["zip"]:
        create_joins_zip(address, module_name, info["body"])

    print("### Create front dimensions ###")
    create_dimensions_front(address, module_name, dms_dump["dimensions"]["front"]["body"])


def import_rafal(address, module_name, data):
    credentials = {"identifier": "guillaume", "password": "Welcome?"}
    #init_user(address, credentials)
    token = login(address, credentials)
    pivots = data["pivots"]

    print("### Create pivots ###")
    for pivot in pivots:
        pivot_name = pivot["name"]
        del pivot["name"]
        pivot_dims = pivot["dimensions"]
        forbidden_keys = dictionaries.dictNames(data)
        for fk in forbidden_keys:
            if fk in pivot_dims :
                pivot_dims.remove(fk)
        create_pivots(address, module_name, pivot_name, token, pivot)


# Rafal Queries


def init_user(address, credentials):
    current_headers = headers.copy()
    current_headers["Authorization"] = "InitiateSuperuser"
    response = requests.post(
        f"{address}/users/init", headers=current_headers, json=credentials
    )
    return response.content


def login(address, credentials):
    response = requests.post(f"{address}/login", headers=headers, json=credentials)
    return json.loads(response.content)["token"]


def create_pivots(address, module, pivot_name, token, body):
    current_headers = headers.copy()
    current_headers["Authorization"] = f"Bearer {token}"
    requests.post(
        f"{address}/module/{module}/pivot/{pivot_name}",
        headers=current_headers,
        json=body,
    )


# DMS Queries


def create_modules(address, module_name):
    requests.post(f"{address}/modules", headers=headers, json={"name": module_name})


def create_tables_fact(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/tables/fact",
        headers=headers,
        json=body,
    )


def create_tables_dict_composite(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/tables/dict/composite",
        headers=headers,
        json=body,
    )


def create_tables_dict_denormalized(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/tables/dict/denormalized",
        headers=headers,
        json=body,
    )


def create_tables_zip(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/tables/zip",
        headers=headers,
        json=body,
    )


def create_dimensions_array(address, module_name, table_name, body):
    requests.post(
        f"{address}/modules/{module_name}/tables/fact/{table_name}/dimensions/array",
        headers=headers,
        json=body,
    )


def create_dimensions_group(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/dimensions/group",
        headers=headers,
        json=body,
    )


def create_dimensions_front(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/dimensions/front",
        headers=headers,
        json=body,
    )


def create_facets(address, module_name, table_name, body):
    requests.post(
        f"{address}/modules/{module_name}/tables/fact/{table_name}/facets",
        headers=headers,
        json=body,
    )


def create_tables_zip_internal(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/tables/zip/internal",
        headers=headers,
        json=body,
    )


def create_joins_dict_composite(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/joins/dict/composite",
        headers=headers,
        json=body,
    )


def create_joins_dict_denormalized(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/joins/dict/denormalized",
        headers=headers,
        json=body,
    )


def create_joins_zip(address, module_name, body):
    requests.post(
        f"{address}/modules/{module_name}/joins/zip",
        headers=headers,
        json=body,
    )


def create_ingestion_settings(address, module_name, table_name, body):
    requests.post(
        f"{address}/modules/{module_name}/tables/{table_name}/ingestion-settings",
        headers=headers,
        json=body,
    )
