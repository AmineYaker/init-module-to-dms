import json
import os

from facttables import dump as dumpFactTables
from dictionaries import dumpDictionaries
from ziptables import dumpZipTables
from denormalizedJoins import dumpDenormalizedJoins
from zipJoins import dumpZipJoins
from customization_factTables import dump_customizations
from generate_dms_dump import generate_dms_dump
from import_dms import check_connection, import_dms, login, init_user, import_rafal


def clean_all(
    fact_tables_path,
    denormalized_dict_tables_path,
    zip_tables_path,
    denormalized_dict_joins_path,
    zip_joins_path,
    customization_path,
    dms_dump_path
):
    def clean_json(file_path):
        if os.path.splitext(file_path)[-1] != ".json":
            raise Exception("dumps directory should only contain json files.")
        if os.path.exists(file_path):
            os.remove(file_path)

    def clean(directory_path):
        if os.path.exists(directory_path):
            file_names = os.listdir(directory_path)
            for file_name in file_names:
                clean_json(os.path.join(directory_path, file_name))
            os.rmdir(directory_path)

    clean(fact_tables_path)
    clean(denormalized_dict_tables_path)
    clean(zip_tables_path)
    clean(denormalized_dict_joins_path)
    clean(zip_joins_path)
    clean(customization_path)
    clean_json(dms_dump_path)


if __name__ == "__main__":
    dms_address = "http://localhost:8085"
    rafal_address = "http://localhost:9000"
    module_name = "sage"
    
    secondary_cluster = 'secondary1'

    sage_file = "SAGE_v8_3_onprem.json"
    fact_tables_path = "./tables_dumps"
    denormalized_dict_tables_path = "./dictionaries_dumps"
    zip_tables_path = "./zips_dumps"
    denormalized_dict_joins_path = "./denormalized_joins_dumps"
    zip_joins_path = "./zip_joins_dumps"
    customization_path = "./customization_dumps"
    dms_dump_path = "./dms_dump.json"

    clean_all(
        fact_tables_path,
        denormalized_dict_tables_path,
        zip_tables_path,
        denormalized_dict_joins_path,
        zip_joins_path,
        customization_path,
        dms_dump_path
    )

    with open(sage_file) as json_file:
        data = json.load(json_file)

        dumpFactTables(data, secondary_cluster)
        dumpDictionaries(data, denormalized_dict_tables_path)
        dumpZipTables(data, zip_tables_path)
        dumpDenormalizedJoins(data, denormalized_dict_joins_path)
        dumpZipJoins(data, zip_joins_path)
        dump_customizations(data, customization_path)
        

    dms_dump = generate_dms_dump(
        module_name,
        fact_tables_path,
        denormalized_dict_tables_path,
        zip_tables_path,
        denormalized_dict_joins_path,
        zip_joins_path,
        customization_path,
        data
    )

    with open(dms_dump_path, "w") as f:
        f.write(json.dumps(dms_dump, indent=2))

    check_connection(dms_address)
    import_dms(dms_address, dms_dump)
    import_rafal(rafal_address, module_name, data)
