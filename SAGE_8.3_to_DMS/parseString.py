import re


def parse_table_string(table_string):
    pattern = r"\((.*)\) ENGINE"
    column_string = re.search(pattern, table_string).group(1)
    # columns = re.findall(r"\w+\s+\w+", column_string)

    # Create a list of dictionaries for each column
    parsed_columns = []
    for column in column_string.split(", "):
        name, data_type = column.split(" ")
        parsed_columns.append({"name": name, "dataType": data_type})

    return parsed_columns


def find_create_statement(table_name, create_statements):
    for statement in create_statements:
        if table_name in statement:
            return statement
    return None

def parse_file_cols(table_string):
    pattern = r"FROM input\('(.*)'\)"
    column_string = re.search(pattern, table_string).group(1)

    # Create a list of dictionaries for each column
    parsed_columns = []
    for column in column_string.split(", "):
        name, data_type = column.split(" ")
        parsed_columns.append({"name": name, "dataType": data_type})

    return parsed_columns

## works only for versioned fact tables
def parse_customizing_images(table_string):
    pattern = r"Date, (.*), ~~commit"
    column_string = re.search(pattern, table_string).group(1)
    # columns = re.findall(r"\w+\s+\w+", column_string)

    # Create a list of dictionaries for each column
    columns =column_string.split(", ")
    return columns




# Example usage
##table_string = "CREATE TABLE IF NOT EXISTS default.Bucketing ON CLUSTER '{cluster}' (Date Date, BucketType String, BucketCode Array(String), BucketCodeUSD Array(String)) ENGINE = ReplicatedMergeTree('{zoo_prefix}/tables/{shard_oneshard}/Bucketing', '{host}') PARTITION BY tuple()  ORDER BY tuple()"
##columns = parse_table_string(table_string)
##for column in columns:
##    print(column)