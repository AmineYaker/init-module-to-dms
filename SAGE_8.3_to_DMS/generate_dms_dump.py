import os
import json

from arrayDimensions import get_array_dimensions_bodies
from frontDimensions import get_front_dimensions_bodies


def load_bodies(directory_name):
    files = os.listdir(directory_name)
    bodies = []
    for file_name in files:
        with open(os.path.join(directory_name, file_name), "r") as file:
            bodies.append(json.load(file))
    return bodies


def generate_dms_dump(
    module_name,
    fact_tables_path,
    denormalized_dict_tables_path,
    zip_tables_path,
    denormalized_dict_joins_path,
    zip_joins_path,
    ingestion_customization_path,
    data
):
    result = {
        "module": module_name,
        "tables": {
            "fact": [],
            "dict": {"composite": [], "denormalized": []},
            "zip": {"external": [], "internal": []},
        },
        "joins": {
            "dict": {"composite": [], "denormalized": []},
            "zip": [],
        },
        "dimensions": {"group": [], "front": {}},
    }

    for body in load_bodies(fact_tables_path):
        fact_table_name = body["name"]
        result["tables"]["fact"].append(
            {
                "dimensions": {"array": [{"body": body} for body in get_array_dimensions_bodies(data, fact_table_name)]},
                "facets": [],
                "ingestion-settings": {},
                "body": body,
            }
        )

    for body in load_bodies(denormalized_dict_tables_path):
        result["tables"]["dict"]["denormalized"].append({"body": body})

    for body in load_bodies(zip_tables_path):
        result["tables"]["zip"]["external"].append({"body": body})

    for body in load_bodies(denormalized_dict_joins_path):
        result["joins"]["dict"]["denormalized"].append({"body": body})

    for body in load_bodies(zip_joins_path):
        result["joins"]["zip"].append({"body": body})

    result["dimensions"]["front"] = {"body": get_front_dimensions_bodies(data)}

    for body in load_bodies(ingestion_customization_path):
        tableName = body["tableName"]
        for factTable in result['tables']['fact']:
            if factTable["body"]["name"] == tableName:
                del body['tableName']
                factTable["ingestion-settings"] = body

    return result
